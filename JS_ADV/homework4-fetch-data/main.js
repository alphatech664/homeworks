let filmsPromise = fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(filmsAll => {
        const globalFilmList = createElement({
            element: 'ul',
            elementToAddFor: document.body,
            where: 'afterbegin',
            classes: ['globalFilmList']
        })

        filmsAll.forEach(filmFull => {
            let filmElem = createElement({
                element: 'li',
                elementToAddFor: globalFilmList,
                where: 'beforeend',
                innerText: filmFull.name,
                classes: ['film']
            })

            let filmDetails = createElement({
                element: 'ul',
                elementToAddFor: filmElem,
                where: 'beforeend',
                innerHtml: `<li><span class="descriptionTitle">Characters:</span>
                                <span class="loader"></span>
                                <span class="characters"></span></li>
                            <li><span class="descriptionTitle">EpisodeId:</span> ${filmFull.episodeId}</li>
                            <li><span class="descriptionTitle">OpeningCrawl:</span> ${filmFull.openingCrawl}</li>`,
                classes: ['filmDescription']
            })
            let filmCharacters = filmDetails.querySelector('.characters');
            let loader = filmDetails.querySelector('.loader');

            Promise.all(filmFull.characters.map(u => fetch(u))).then(responses =>
                Promise.all(responses.map(res => res.json()))
            ).then(characters => {
                let allCharactersOfFilm = [];
                characters.forEach(character => {
                    allCharactersOfFilm.push(character.name)
                })
                allCharactersOfFilm = JSON.stringify(allCharactersOfFilm.join(', ')).slice(1, -1).replace(/"/g, '');
                loader.remove();
                filmCharacters.innerText = filmCharacters.innerText + ' ' + allCharactersOfFilm;
            })
        })
    })

function createElement({ element, elementToAddFor, where, innerText = '', innerHtml, classes = [] }) {
    let newElement = document.createElement(element);
    elementToAddFor.insertAdjacentElement(where, newElement);
    innerHtml ? newElement.innerHTML = innerHtml : newElement.innerText = innerText;
    classes.length ? classes.forEach(className => newElement.classList.add(className)) : newElement.classList;
    return newElement;
}