class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }
    get name() {
        return this._name;
    }

    get age() {
        return this._age;
    }

    get salary() {
        return this._salary;
    }

    set name(value) {
        this._name = value;
    }

    set age(value) {
        this._age = value;
    }

    set salary(value) {
        this._salary = value;
    }
}

let Manager = new Employee('Bohdan', '19', '600$');
console.log(Manager);


class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary, lang);
        this._name = name;
        this._age = age;
        this._salary = salary;
        this._lang = lang;
    }

    get lang() {
        return this._lang;
    }

    set lang(value) {
        this._lang = value;
    }

}
let Senior = new Programmer('Igor', '29', '600$', 'Ukrainian')
let Junior = new Programmer('Mark', '29', '600$', 'Ukrainian')
let Middle = new Programmer('Alex', '29', '600$', 'Ukrainian')
console.log(Senior);
console.log(Junior);
console.log(Middle);