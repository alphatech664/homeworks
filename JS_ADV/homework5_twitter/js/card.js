import Element from './element.js';


class Card extends Element {
    constructor(name, username, cardId, title, body) {
        super()
        this.name = name;
        this.username = username;
        this.cardId = cardId;
        this.title = title;
        this.body = body
    }

    renderHeader() {
        const header = this.createElement('div', ['card-header'])
        header.innerHTML = `
            <div class="card-header__author author">
                <div class="author__img"></div>
                <div class="author__info">
                    <div class="author__name author__info_bold">${this.name}</div>
                    <div class="author__username">@${this.username}</div>
                </div>
            </div>
            <button class="card-header__btn--delete btn btn-primary">X</button>
        `
        header.addEventListener('click', (e) => {
            const delTarget = e.target.closest('.card-header__btn--delete');
            if (delTarget) {
                const toDelCard = confirm('Delete Card?');
                if (toDelCard) {
                    delTarget.closest('.post-card').remove();
                    fetch(`https://ajax.test-danit.com/api/json/posts/${this.cardId}`, {
                        method: 'DELETE'
                    })
                }
            }
        })
        return header;
    }

    renderBody() {
        const cardBody = this.createElement('div', ['card-body'])
        cardBody.innerHTML = `
        <p class="card-body__content">${this.body}</p>
        <div class="time-info">
            <span class="time-info__hours">10:13 PM &#183;</span>
            <span class="time-info__date"> Apr 6, 2022 &#183;</span>
            <span class="source"> Twitter Web App</span>
        </div>
        `
        return cardBody;
    }

    renderFooter() {
        const cardFooter = this.createElement('div', ['card-footer'])
        cardFooter.innerHTML = `
        <p class="reactions"><span class="reactions_bold">${randomNumber()}</span> Retweets</p>
        <p class="reactions"><span class="reactions_bold">${randomNumber()}</span> Quote Tweets</p>
        <p class="reactions"><span class="reactions_bold">${randomNumber()}</span> Likes</p>
        `
        return cardFooter;
    }

    render() {
        this.postCard = this.createElement('div', ['post-card']);
        this.postCard.append(this.renderHeader(), this.renderBody(), this.renderFooter());
        return this.postCard;
    }
}

function randomNumber() {
    return (Math.round(0 - 0.5 + Math.random() * (1001)))
}

export default Card