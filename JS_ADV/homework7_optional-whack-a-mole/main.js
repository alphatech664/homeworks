const rootDiv = document.getElementById('root');
const startBtn = document.querySelector('.startBtn');
const levelBtn = document.querySelector('.levelBtn');
const chosenLevel = document.querySelector('.chosenLevel');
const timerSecElem = document.querySelector('.timer_sec');
const timerMSecElem = document.querySelector('.timer_msec');

const table = createSqureTable(10, rootDiv);

startBtn.onclick = () => {
    if (startBtn.innerHTML.toLowerCase() === 'stop') {
        startBtn.innerHTML = 'Start';
    } else if (startBtn.innerHTML.toLowerCase() === 'start') {
        levelBtn.classList.toggle('levelBtn-shown');
    };
}

levelBtn.onclick = (e) => {
    if (e.target.tagName !== 'BUTTON') {
        return;
    }
    const chosenLevelBtn = e.target;
    chosenLevel.innerText = chosenLevelBtn.innerText;
    startBtn.innerText = 'Stop';
    levelBtn.classList.remove('levelBtn-shown');
}

function createSqureTable(length, placeToAdd) {
    const tableElem = document.createElement('table');
    placeToAdd.insertAdjacentElement('beforeend', tableElem);
    let cellData = 0;
    for (let i = 0; i < length; i++) {
        let tableRow = document.createElement('tr');
        tableElem.insertAdjacentElement('beforeend', tableRow);
        for (let y = 1; y <= length; y++) {
            cellData++;
            let cell = document.createElement('td');
            tableRow.insertAdjacentElement('beforeend', cell);
            cell.innerText = cellData;
            cell.setAttribute('id', `cell_${cellData}`);
        }
    }
    const table = document.querySelector('table');
    return table;
}

function createArrayWithCellId(length) {
    let arrayWithId = [];
    for (let i = 1; i <= length; i++) {
        arrayWithId.push(`cell_${i}`)
    }
    return arrayWithId;
}

function colorCell(sec = 100, length = 100) {
    let coloredId = [];
    let i = 0;
    const colorCellInt = setInterval(() => {
        i++;
        console.log(i);
        [newCellId, coloredId] = generateUniqueNumber(coloredId);
        const newCell = document.getElementById(`cell_${newCellId}`);
        console.log(newCell);
        newCell.classList.add('cell_colored');
        if (i === length) {
            clearInterval(colorCellInt);
        }
    }, sec)

}
colorCell();

function generateUniqueNumber(arr = [], maxNum = 101) {
    let number;
    do {
        number = Math.floor(Math.random() * (maxNum - 1)) + 1;
    } while (arr.indexOf(number) !== -1)
    arr.push(number);
    return [number, arr];
}

// function startTimer(sec, placeToUpdateSec, placeToUpdateMSec) {
//     placeToUpdateSec.innerText = sec;
//     placeToUpdateMSec.innerText = 90;
//     window.timerTime = setInterval(() => {
//         placeToUpdateMSec.innerText = +placeToUpdateMSec.innerText - 10;
//         if (+placeToUpdateMSec.innerText <= 0) {
//             placeToUpdateSec.innerText = +placeToUpdateSec.innerText - 1;
//             placeToUpdateMSec.innerText = 90
//         }
//         if (+placeToUpdateSec.innerText === 0) {
//             clearInterval(timerTime);
//             startTimer(sec, placeToUpdateSec, placeToUpdateMSec)
//         }
//     }, 100);
// }