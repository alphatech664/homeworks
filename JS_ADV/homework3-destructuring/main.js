//#1

// const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
// const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];
//
// const [gilbert,,, ...other] = clients1
// // console.log(gilbert);
// // console.log(other);
// let clientsNew = gilbert+','+other+','+clients2
// console.log(clientsNew);

// #2
//
//
// const characters = [
//     {
//         name: "Елена",
//         lastName: "Гилберт",
//         age: 17,
//         gender: "woman",
//         status: "human"
//     },
//     {
//         name: "Кэролайн",
//         lastName: "Форбс",
//         age: 17,
//         gender: "woman",
//         status: "human"
//     },
//     {
//         name: "Аларик",
//         lastName: "Зальцман",
//         age: 31,
//         gender: "man",
//         status: "human"
//     },
//     {
//         name: "Дэймон",
//         lastName: "Сальваторе",
//         age: 156,
//         gender: "man",
//         status: "vampire"
//     },
//     {
//         name: "Ребекка",
//         lastName: "Майклсон",
//         age: 1089,
//         gender: "woman",
//         status: "vampire"
//     },
//     {
//         name: "Клаус",
//         lastName: "Майклсон",
//         age: 1093,
//         gender: "man",
//         status: "vampire"
//     }
// ];
//
// const {name:name0, lastName:lastName0, age:ages0, ...other0} = characters[0]
// const {name:name1, lastname:lastName1, age:ages1, ...other1} = characters[1]
// const {name:name2, lastname:lastName2, age:ages2, ...other2} = characters[2]
// const {name:name3, lastname:lastName3, age:ages3, ...other3} = characters[3]
// const {name:name4, lastname:lastName4, age:ages4, ...other4} = characters[4]
// const {name:name5, lastname:lastName5, age:ages5, ...other5} = characters[5]
//
// let charactersShortInfo = [{name0, lastName0, ages0},{name1, lastName1, ages1}, {name2, lastName2, ages2}, {name3, lastName3, ages3}, {name4, lastName4, ages4}, {name5, lastName5, ages5}]
// console.log(charactersShortInfo);

// #3

// const user1 = {
//     name: "John",
//     years: 30
//     // isAdmin:false
// };
//
// const {
//     name:{Name},
//     years:{Years},
//     isAdmin = 'false'
// } = user1
//
// console.log(user1);
// console.log(isAdmin);

// #4
//
// const satoshi2020 = {
//     name: 'Nick',
//     surname: 'Sabo',
//     age: 51,
//     country: 'Japan',
//     birth: '1979-08-21',
//     location: {
//         lat: 38.869422,
//         lng: 139.876632
//     }
// }
//
// const satoshi2019 = {
//     name: 'Dorian',
//     surname: 'Nakamoto',
//     age: 44,
//     hidden: true,
//     country: 'USA',
//     wallet: '1A1zP1eP5QGefi2DMPTfTL5SLmv7DivfNa',
//     browser: 'Chrome'
// }
//
// const satoshi2018 = {
//     name: 'Satoshi',
//     surname: 'Nakamoto',
//     technology: 'Bitcoin',
//     country: 'Japan',
//     browser: 'Tor',
//     birth: '1975-04-05'
// }

// const fullProfile = {...satoshi2018, ...satoshi2019, ...satoshi2020 };
// console.log(fullProfile);

// #5

// const books = [{
//     name: 'Harry Potter',
//     author: 'J.K. Rowling'
// }, {
//     name: 'Lord of the rings',
//     author: 'J.R.R. Tolkien'
// }, {
//     name: 'The witcher',
//     author: 'Andrzej Sapkowski'
// }];
//
// const bookToAdd = {
//     name: 'Game of thrones',
//     author: 'George R. R. Martin'
// }
//
// let allBooks = [...books, bookToAdd]
// console.log(allBooks);

// #6

// const employee = {
//     name: 'Vitalii',
//     surname: 'Klichko'
// }
//
// const newParams = {
//     age: '50',
//     salary: '100000$',
// }
//
// let newEmploee = {...employee, ...newParams}
// console.log(newEmploee);

// #7

// const array = ['value', () => 'showValue'];
// // Допишіть код тут
// value = 'value';
//  function showValue() {
//     const [, showValue] = array
//  }
//
//
// alert(value); // має бути виведено 'value'
// alert(showValue());  // має бути виведено 'showValue'