const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function addList(arr, parent = document.body) {
    parent.innerHTML += `<ul>${arr.map(item => <li>${item}</li>).join('')}</ul>`;
}
addList(arr);