export default class ModalWindowDeclarations {

  constructor() {
    
    this.modalWindowDeclarations = [
      {
        id: 'modalID1',
        title: "title for modal 1",
        description: 'description for modal 1',
        closeBtn: true,
        backgroundColor: "red"
      },
      {
        id: 'modalID2',
        title: "title for modal 2",
        description: 'description for modal 2',
        closeBtn: false,
        backgroundColor: "green"
      },
      {
        id: 'modalID3',
        title: "Add to cart",
        description: "Are you sure you want to add this item to your cart?",
        closeBtn: false,
        backgroundColor: "cadetblue"
      },
      {
        id: 'modalID4',
        title: "Remove from cart",
        description: "Are you sure you want to remove this item from your cart?",
        closeBtn: false,
        backgroundColor: "cadetblue"
      }
    ]
  }

  findWindowById(dataModalId){
    return this.modalWindowDeclarations.find(window => window.id === dataModalId);
  }
}

  
  