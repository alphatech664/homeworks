import React from "react";

export default function CartProductForPopUp ({id, name, price, pictureUrl,toggleProductInCart}) {

        return(
            <>
                <li>
                    <div className="cart-block-item">
                        <div className="cart-product">
                            <span className="cart-product__price">$ {price}</span>
                            <span className="cart-product__name">{name}</span>
                            <img className="cart-product__image" src={pictureUrl} alt={name} />
                        </div>
                        <button onClick={()=>toggleProductInCart({id})} className="cart-block-item__btn-rm">X</button>
                    </div>
                </li>
            </>
        )
    

}