import React, {Component} from "react";
import CartProduct from "./CartProduct";
import './Cart.scss'

export default class CartList extends Component{

    render(){
        const {products, toggleCartShowing, toggleProductInLocalStorageCart} = this.props;
        
        const totalPrice = products.reduce((acc, product) => acc+product.price, 0);

        return(
            
           
            <>
                <div className="cart-pop-up" onMouseLeave={() => toggleCartShowing(false)}>
                    <ul className="cart-block">
                        {products.map(product => <CartProduct key={product.id+"---cart"} {...product} toggleProductInLocalStorageCart={toggleProductInLocalStorageCart}/>)}
                    </ul>
                    <div className="cartFooter">
                        <p className="cartFooter__total-amount">Total cost $ {totalPrice}</p>
                        <div className="cartFooter__actionButtons">
                            <a href="./CartList#">View Cart</a>
                            <a href="./CartList#">Proceed to Checkout</a>
                        </div>
                    </div>
                </div>
            </>
        )
    }
    
}