import React, {Component} from "react";

export default class CartProduct extends Component{

    render(){

        const {id, name, price, pictureUrl,toggleProductInLocalStorageCart} = this.props;

        return(
            <>
                <li>
                    <div className="cart-block-item">
                        <div className="cart-product">
                            <span className="cart-product__price">$ {price}</span>
                            <span className="cart-product__name">{name}</span>
                            <img className="cart-product__image" src={pictureUrl} alt={name} />
                        </div>
                        <button onClick={()=>toggleProductInLocalStorageCart({id})} className="cart-block-item__btn-rm">X</button>
                    </div>
                </li>
            </>
        )
    }

}