import React from "react";
import CartProductForPopUp from "./CartProductForPopUp";
import './Cart.scss'

export default function CartListForPopUp (props) {

        const {productsInCart, toggleCartShowing, toggleProductInCart} = props;
        const totalPrice = productsInCart.reduce((acc, product) => acc+product.price, 0);

        return(
            
            <>
                <div className="cart-pop-up" onMouseLeave={() => toggleCartShowing(false)}>
                    <ul className="cart-block">
                        {productsInCart.map(product => <CartProductForPopUp key={product.id+"---cart"} {...product} toggleProductInCart={toggleProductInCart}/>)}
                    </ul>
                    <div className="cartFooter">
                        <p className="cartFooter__total-amount">Total cost $ {totalPrice}</p>
                        <div className="cartFooter__actionButtons">
                            <a href="/cart">View Cart</a>
                            <a href="#">Proceed to Checkout</a>
                            
                        </div>
                    </div>
                </div>
            </>
        )
    
}