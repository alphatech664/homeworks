import React, {Component} from 'react';
import './ButtonGenerateModal.scss';

export default function ButtonGenerateModal (props) {

        const {btnText, dataModalId, actionOnOk, toggleModal, paramsForAction, disabled} = props;
          
        return(
            <button disabled={disabled} className="OpenModalBtn" onClick={()=>toggleModal(dataModalId,actionOnOk,paramsForAction)}>{btnText}</button>
        );
    }
