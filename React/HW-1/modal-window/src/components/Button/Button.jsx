import React, {Component} from 'react';
import './Button.scss'
export default class Button extends Component {

    openModal = (dataModalId) => {
        this.props.onClickShowModal(dataModalId);
    }

    render(){
        const {btnText,dataModalId} = this.props;
        
        return(
            <button className="OpenModalBtn" onClick={()=>this.openModal(dataModalId)}>{btnText}</button>
        );
    }
}