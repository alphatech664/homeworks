import React, {Component} from 'react';
import FavoriteCard from './FavoriteCard';

export default function FavoritesList ({toggleProductInLocalStorageFavorite}) {

        let products = [];

        return(
            <>
            
            <p>My Favourite Products</p>
                <ul className="favorites-block">
                    {products.map(product => 
                    <FavoriteCard key={product.id+"---favorite"} {...product} toggleProductInLocalStorageFavorite={toggleProductInLocalStorageFavorite}
                    />)}
                </ul>
            </>  
        )
    }
