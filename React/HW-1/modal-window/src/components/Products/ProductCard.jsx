import React, { useState, useEffect } from "react";
import ButtonGenerateModal from "../Button/ButtonGenerateModal";
import './ProductCard.scss'
import Modal from "../Modal/Modal.jsx";
import LikeButtonIcon from "../icons/LikeButtonIcon";

export default function ProductCard (props) {

    const {product, toggleModal, toggleFavoriteProduct, toggleProductInCart} = props;

    const checkLocalStorage = (storageName) => {
        const localStorageName = JSON.parse(localStorage.getItem(storageName)) || [];
        const isProductInStorage = localStorageName.find(item => item.id === product.id);
        return isProductInStorage;
    }

    const isFavorite = product.isFavorite ? product.isFavorite : checkLocalStorage('favoriteProducts');
    const isInCart = product.isInCart ? product.isInCart : checkLocalStorage('productsInCart');

        return(
            <>
                <li>
                    <div className="product">
                        <img className="product__image" src={product.pictureUrl} alt={product.name}/>
                        <div className="product__info">
                            <div> 
                                <p className="product__name">{product.name}</p>
                                <span onClick={() => toggleFavoriteProduct(product)}>
                                    <LikeButtonIcon fill={isFavorite  ? "red" : ''}/>
                                </span>
                            </div>
                            <p className="product__description-color">{product.color}</p>
                            <div>
                            <p className="product__price">$ {product.price}</p>
                            {<ButtonGenerateModal
                                dataModalId={isInCart ? 'modalID4' : 'modalID3'}
                                btnText={isInCart ? "Already in Cart" : "Add to Cart"}
                                className={isInCart ? "product__btnAddToCart" : "product__btnAddToCart product__btnAddToCart-disabled"}
                                // disabled = {isInCart}
                                toggleModal={toggleModal}
                                actionOnOk={toggleProductInCart}
                                paramsForAction={product}
                            />}
                            </div>
                        </div>
                    </div>
                </li>
            </>
        )
    
}