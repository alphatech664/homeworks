import * as api from "../../api/goods";
import {GET_PRODUCTS_INIT,ADD_PRODUCTS_IN_FAVORITES,ADD_PRODUCTS_TO_CART,REMOVE_PRODUCTS_FROM_CART,REMOVE_PRODUCTS_FROM_FAVORITES,UPDATE_FROM_LS, MAKE_PURCHASE} from "../../types/types"




export const getRequest=()=> async (dispatch)=>{
    try{
        const {data} = await api.getAllProducts()
        if(!localStorage.getItem('cart')||!localStorage.getItem('favorite')){
            localStorage.setItem('cart',JSON.stringify([]))
            localStorage.setItem('favorite',JSON.stringify([]))
        }
        dispatch({type:GET_PRODUCTS_INIT,payload: data})

    }catch(error){
        console.log(error)
    }

}


export const addToFavorite=(data)=> (dispatch)=>{
    try{
//console.log(data)
        dispatch({type: ADD_PRODUCTS_IN_FAVORITES,payload: data})

    }catch(error){
        console.log(error)
    }

}

export const addToCart=(data)=> (dispatch)=>{
    try{
        //console.log(data)
        dispatch({type: ADD_PRODUCTS_TO_CART,payload: data})
    }catch(error){
        console.log(error)
    }
}

export const removeFromCart=(data)=> (dispatch)=>{
    try{
        console.log(data)
        dispatch({type: REMOVE_PRODUCTS_FROM_CART, payload: data})
    }catch(error){
        console.log(error)
    }
}


export const removeFromFavorite=(data)=>(dispatch)=>{
    try{
        dispatch({type: REMOVE_PRODUCTS_FROM_FAVORITES, payload: data})
    }catch(error){
        console.log(error)
    }
}


export const updateLocalStorage=()=>(dispatch)=>{

    try{
        if(localStorage.getItem('cart')){
            const cart =JSON.parse(localStorage.getItem('cart'))
            const favorite = JSON.parse(localStorage.getItem('favorite'))
            const data ={'cart':cart,
                'favorite':favorite}
            // console.log(data)
            dispatch({type: UPDATE_FROM_LS, payload: data})
        }

    }catch(error){
        console.log(error)
    }
}

export const makePurchase=(data)=>(dispatch)=>{
    try{
        dispatch({type: MAKE_PURCHASE, payload: data})

    }catch(error){
        console.log(error)
    }
}

