const postTest = ({ payload, posts }) => {
    return {
        payload,
        posts
    }
}

export default postTest