import addFavorite from '../favorite.test';

describe('add Favorite', () => {
    it('addin favorite product', () => {
        jest.spyOn(this.state,'now')
        expect(addFavorite({name: 'Bohdan', duration: '96min'})).toEqual({name: 'Bohdan', duration: '96min'})
    })
})