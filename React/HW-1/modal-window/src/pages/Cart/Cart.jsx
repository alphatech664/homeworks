import React from 'react';
import ProductList from '../../components/Products/ProductList';
import Form from '../../components/Form/Form.formik';


const Cart = ({isLoading, products, toggleModal, toggleFavoriteProduct, toggleProductInCart}) => {
    return(

        <div className='cart-container'>
        <ProductList
          isLoading={isLoading}
          products={products}
          toggleModal={toggleModal}
          toggleFavoriteProduct={toggleFavoriteProduct}
          toggleProductInCart={toggleProductInCart}
        />
            <Form/>
        </div>


)
}

export default Cart